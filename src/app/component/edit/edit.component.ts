import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  @Input() data;

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit(): void {
    console.log(this.data)
  }

  edit(form: NgForm) {
    console.log(form.value);
    this.activeModal.close(form.value);
  }

}
