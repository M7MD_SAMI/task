import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditComponent } from './component/edit/edit.component';

interface info {
  date:object,
  name: string,
  school: string,
  nid: number
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  date: NgbDateStruct;
  table:info[] = [];
  Fcolor:string;
  bgColoer:string;
  show:boolean = false;
  num:number;

  constructor(private modalService: NgbModal) {
  }
  
  ngOnInit(): void {
    this.randomNum(3, 2);
  }

  add(form: NgForm) {
    let data = form.value;
    this.table = [...this.table, data];
  }

  delete(index) {
    this.table.splice(index, 1)
  }

  edit() {

  }

  async open(index) {
    const modalRef = this.modalService.open(EditComponent);
    modalRef.componentInstance.data = this.table[index];
    let res = await modalRef.result;
    this.table[index] = res;
  }

  showData() {
    this.show = !this.show;
  } 

  randomNum(length, num) {
    let max = +(9 + '0'.repeat(length - 1));
    let min = +(1 + '0'.repeat(length - 1));
    let random = Math.floor(min + Math.random() * max);
    if(random % num == 0) {
      this.num = random;
      return random;
    }
    this.randomNum(length, num)
  }

}
